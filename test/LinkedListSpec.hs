{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedLists #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module LinkedListSpec (listSpec) where

import Data.List (sort)
import GHC.Exts (IsList (..))
import LinkedList (Node (..), isPalindrome, isPalindromeFast, kToLast, kToLastRunner, partition, removeDup, removeDupNoBuff, reverseList, sumLists, sumListsInOrder)
import Test.HUnit (Test (..), assertBool, assertEqual)
import Test.HUnit.Base (assert)
import Test.QuickCheck (Arbitrary, Property, arbitrary, quickCheck, (===))
import Test.QuickCheck.Gen (Gen)

instance Arbitrary a => Arbitrary (Node a) where
  arbitrary :: Gen (Node a)
  arbitrary = do
    value <- arbitrary
    next <- arbitrary
    pure Node {value, next}

listSpec :: Test
listSpec =
  TestList
    [ reverseListSpec,
      removeDupSpec,
      removeDupNoBuffSpec,
      kToLastSpec,
      kToLastRunnerSpec,
      partitionSpec,
      sumListsSpec,
      sumListsInOrderSpec,
      isPalindromeSpec,
      isPalindromeFastSpec
    ]

reverseListSpec :: Test
reverseListSpec =
  TestList
    [ TestCase $
        assert $
          quickCheck ((\l -> reverseList (reverseList l) === l) :: Node Int -> Property),
      TestCase $ assertEqual "ABCD list" ['D', 'C', 'B', 'A'] $ reverseList ['A', 'B', 'C', 'D']
    ]

removeDupSpec :: Test
removeDupSpec =
  TestList
    [ TestCase $ assertEqual "ABCD list" ['A', 'B', 'C', 'D'] $ removeDup ['A', 'B', 'C', 'D'],
      TestCase $ assertEqual "AAAA list" ['A'] $ removeDup ['A', 'A', 'A', 'A'],
      TestCase $ assertEqual "ABBA list" ['A', 'B'] $ removeDup ['A', 'B', 'B', 'A'],
      TestCase $ assertEqual "A list" ['A'] $ removeDup ['A'],
      TestCase $
        assert $
          quickCheck ((\l -> removeDup (removeDup l) === removeDup l) :: Node Int -> Property)
    ]

removeDupNoBuffSpec :: Test
removeDupNoBuffSpec =
  TestList
    [ TestCase $ assertEqual "ABCD list" ['A', 'B', 'C', 'D'] $ removeDupNoBuff ['A', 'B', 'C', 'D'],
      TestCase $ assertEqual "AAAA list" ['A'] $ removeDupNoBuff ['A', 'A', 'A', 'A'],
      TestCase $ assertEqual "ABBA list" ['B', 'A'] $ removeDupNoBuff ['A', 'B', 'B', 'A'],
      TestCase $ assertEqual "A list" ['A'] $ removeDupNoBuff ['A'],
      TestCase $
        assert $
          quickCheck ((\l -> removeDupNoBuff (removeDupNoBuff l) === removeDupNoBuff l) :: Node Int -> Property),
      TestCase $
        assert $
          quickCheck ((\l -> sort (toList $ removeDupNoBuff l) === sort (toList $ removeDup l)) :: Node Int -> Property)
    ]

kToLastSpec :: Test
kToLastSpec =
  TestList
    [ TestCase $ assertEqual "last element of single element list" (Just [1]) $ kToLast 0 [1 :: Int],
      TestCase $ assertEqual "1 to last element of single element list" Nothing $ kToLast 1 [1 :: Int],
      TestCase $ assertEqual "1 to last element of a list" (Just [2, 3]) $ kToLast 1 [1 :: Int, 2, 3],
      TestCase $ assertEqual "2 to last element of a list" (Just [2, 3, 4]) $ kToLast 2 [1 :: Int, 2, 3, 4],
      TestCase $ assertEqual "10 to last element of a shorter list" Nothing $ kToLast 10 [1 :: Int, 2, 3, 4]
    ]

kToLastRunnerSpec :: Test
kToLastRunnerSpec =
  TestList
    [ TestCase $ assertEqual "last element of single element list" (Just [1]) $ kToLastRunner 0 [1 :: Int],
      TestCase $ assertEqual "1 to last element of single element list" Nothing $ kToLastRunner 1 [1 :: Int],
      TestCase $ assertEqual "1 to last element of a list" (Just [2, 3]) $ kToLastRunner 1 [1 :: Int, 2, 3],
      TestCase $ assertEqual "2 to last element of a list" (Just [2, 3, 4]) $ kToLastRunner 2 [1 :: Int, 2, 3, 4],
      TestCase $ assertEqual "10 to last element of a shorter list" Nothing $ kToLastRunner 10 [1 :: Int, 2, 3, 4],
      TestCase $
        assert $
          quickCheck
            ((\i l -> kToLast i l === kToLastRunner i l) :: Int -> Node Int -> Property)
    ]

partitionSpec :: Test
partitionSpec =
  TestList
    [ TestCase $ assertEqual "45871 with 5" [4, 1, 7, 8, 5] $ partition 5 [4, 5, 8, 7, 1 :: Int],
      TestCase $ assertEqual "45871 with 4" [1, 7, 8, 5, 4] $ partition 4 [4, 5, 8, 7, 1 :: Int],
      TestCase $ assertEqual "741852963 with 5" [4, 1, 2, 3, 6, 9, 5, 8, 7] $ partition 5 [7, 4, 1, 8, 5, 2, 9, 6, 3 :: Int],
      TestCase $ assertEqual "1 with 1" [1] $ partition 1 [1 :: Int],
      TestCase $ assertEqual "1 with 0" [1] $ partition 0 [1 :: Int],
      TestCase $ assertEqual "10 with 1" [0, 1] $ partition 1 [1, 0 :: Int]
    ]

sumListsSpec :: Test
sumListsSpec =
  TestList
    [ TestCase $ assertEqual "617 + 295" [2, 1, 9] $ sumLists [7, 1, 6] [5, 9, 2],
      TestCase $ assertEqual "9876 + 945" [1, 2, 8, 0, 1] $ sumLists [6, 7, 8, 9] [5, 4, 9],
      TestCase $ assertEqual "879 + 586" [5, 6, 4, 1] $ sumLists [9, 7, 8] [6, 8, 5]
    ]

sumListsInOrderSpec :: Test
sumListsInOrderSpec =
  TestList
    [ TestCase $ assertEqual "617 + 295" [9, 1, 2] $ sumListsInOrder [6, 1, 7] [2, 9, 5],
      TestCase $ assertEqual "9876 + 945" [1, 0, 8, 2, 1] $ sumListsInOrder [9, 8, 7, 6] [9, 4, 5],
      TestCase $ assertEqual "879 + 586" [1, 4, 6, 5] $ sumListsInOrder [8, 7, 9] [5, 8, 6]
    ]

isPalindromeSpec :: Test
isPalindromeSpec =
  TestList
    [ TestCase $ assertBool "1234" $ not $ isPalindrome [1, 2, 3, 4 :: Int],
      TestCase $ assertBool "1221" $ isPalindrome [1, 2, 2, 1 :: Int],
      TestCase $ assertBool "12321" $ isPalindrome [1, 2, 3, 2, 1 :: Int]
    ]

isPalindromeFastSpec :: Test
isPalindromeFastSpec =
  TestList
    [ TestCase $ assertBool "1234" $ not $ isPalindromeFast [1, 2, 3, 4 :: Int],
      TestCase $ assertBool "1221" $ isPalindromeFast [1, 2, 2, 1 :: Int],
      TestCase $ assertBool "12321" $ isPalindromeFast [1, 2, 3, 2, 1 :: Int],
      TestCase $ assertBool "131" $ isPalindromeFast [1, 3, 1 :: Int],
      TestCase $
        assert $
          quickCheck
            ((\l -> isPalindrome l === isPalindromeFast l) :: Node Int -> Property)
    ]
