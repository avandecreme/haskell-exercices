module ArraysStringsSpec (arraysStringSpec) where

import ArraysStrings (allUniques, allUniquesNoSet, areOneEditAway, checkPermutations, compress, isPalindromePermutation, isStringRotation, rotateMatrix, urlify, zeroMatrix)
import Test.HUnit (Test (TestCase, TestList), assert, assertBool, assertEqual)
import Test.QuickCheck (quickCheck)

arraysStringSpec :: Test
arraysStringSpec =
  TestList
    [ allUniquesSpec,
      allUniquesNoSetSpec,
      checkPermutationsSpec,
      urlifySpec,
      isPalindromePermutationSpec,
      areOneEditAwaySpec,
      compressSpec,
      rotateMatrixSpec,
      zeroMatrixSpec,
      isStringRotationSpec
    ]

allUniquesSpec :: Test
allUniquesSpec =
  TestList
    [ TestCase $ assertBool "empty string" $ allUniques "",
      TestCase $ assertBool "abcd" $ allUniques "abcd",
      TestCase $ assertBool "aabb" $ not $ allUniques "aabb"
    ]

allUniquesNoSetSpec :: Test
allUniquesNoSetSpec =
  TestList
    [ TestCase $ assertBool "empty string" $ allUniquesNoSet "",
      TestCase $ assertBool "abcd" $ allUniquesNoSet "abcd",
      TestCase $ assertBool "aabb" $ not $ allUniquesNoSet "aabb",
      TestCase $ assert $ quickCheck (\s -> allUniques s == allUniquesNoSet s)
    ]

checkPermutationsSpec :: Test
checkPermutationsSpec =
  TestList
    [ TestCase $ assertBool "empty string" $ checkPermutations "" "",
      TestCase $ assertBool "abc bca" $ checkPermutations "abc" "bca",
      TestCase $ assertBool "qwe asd" $ not $ checkPermutations "qwe" "asd",
      TestCase $ assertBool "aaab aab" $ not $ checkPermutations "aaab" "aab",
      TestCase $ assertBool "aaab aabb" $ not $ checkPermutations "aaab" "aabb",
      TestCase $ assertBool "aab aba" $ checkPermutations "aab" "aba"
    ]

urlifySpec :: Test
urlifySpec =
  TestList
    [ TestCase $ assertEqual "empty string" "" (urlify "" 0),
      TestCase $ assertEqual "hello" "hello" (urlify "hello" 5),
      TestCase $ assertEqual "hello world" "hello%20world" (urlify "hello world  " 11),
      TestCase $ assertEqual "pif paf pouf" "pif%20paf%20pouf" (urlify "pif paf pouf    " 12),
      TestCase $ assertEqual "Mr John Smith" "Mr%20John%20Smith" (urlify "Mr John Smith    " 13)
    ]

isPalindromePermutationSpec :: Test
isPalindromePermutationSpec =
  TestList
    [ TestCase $ assertBool "hello" $ not $ isPalindromePermutation "hello",
      TestCase $ assertBool "helleh" $ isPalindromePermutation "helleh",
      TestCase $ assertBool "Tact Coa" $ isPalindromePermutation "Tact Coa"
    ]

areOneEditAwaySpec :: Test
areOneEditAwaySpec =
  TestList
    [ TestCase $ assertBool "pale ple" $ areOneEditAway "pale" "ple",
      TestCase $ assertBool "ple pale" $ areOneEditAway "ple" "pale",
      TestCase $ assertBool "pales pale" $ areOneEditAway "pales" "pale",
      TestCase $ assertBool "pale bale" $ areOneEditAway "pale" "bale",
      TestCase $ assertBool "pale pale" $ areOneEditAway "pale" "pale",
      TestCase $ assertBool "pale bake" $ not $ areOneEditAway "pale" "bake"
    ]

compressSpec :: Test
compressSpec =
  TestList
    [ TestCase $ assertEqual "aabcccccaaa" "a2b1c5a3" $ compress "aabcccccaaa",
      TestCase $ assertEqual "abcd" "abcd" $ compress "abcd",
      TestCase $ assertEqual "aabbbcdd" "aabbbcdd" $ compress "aabbbcdd",
      TestCase $ assertEqual "empty string" "" $ compress ""
    ]

rotateMatrixSpec :: Test
rotateMatrixSpec =
  TestList
    [ TestCase $ assertEqual "1x1" [['A']] $ rotateMatrix [['A']],
      TestCase $
        assertEqual "2x2" [['C', 'A'], ['D', 'B']] $
          rotateMatrix [['A', 'B'], ['C', 'D']],
      TestCase $
        assertEqual "3x3" [['G', 'D', 'A'], ['H', 'E', 'B'], ['I', 'F', 'C']] $
          rotateMatrix [['A', 'B', 'C'], ['D', 'E', 'F'], ['G', 'H', 'I']]
    ]

zeroMatrixSpec :: Test
zeroMatrixSpec =
  TestList
    [ TestCase $
        assertEqual "no zero" [[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12]] $
          zeroMatrix [[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12]],
      TestCase $
        assertEqual "no zero" [[1, 0, 3], [0, 0, 0], [7, 0, 9], [10, 0, 12]] $
          zeroMatrix [[1, 2, 3], [4, 0, 6], [7, 8, 9], [10, 11, 12]]
    ]

isStringRotationSpec :: Test
isStringRotationSpec =
  TestList
    [ TestCase $ assertBool "empty string" $ isStringRotation "" "",
      TestCase $ assertBool "water bottle" $ not $ isStringRotation "water" "bottle",
      TestCase $ assertBool "water bottle" $ isStringRotation "water" "water",
      TestCase $ assertBool "waterbottle erbottlewat" $ isStringRotation "waterbottle" "erbottlewat",
      TestCase $ assertBool "waterbottle erbottlewater" $ not $ isStringRotation "waterbottle" "erbottlewater"
    ]
