import ArraysStringsSpec (arraysStringSpec)
import LinkedListSpec (listSpec)
import Test.HUnit (Test (TestLabel, TestList), runTestTTAndExit)

main :: IO ()
main =
  runTestTTAndExit $
    TestList
      [ TestLabel "linkedListSpec" listSpec,
        TestLabel "arraysStringSpec" arraysStringSpec
      ]
