module ArraysStrings
  ( allUniques,
    allUniquesNoSet,
    checkPermutations,
    urlify,
    isPalindromePermutation,
    areOneEditAway,
    compress,
    rotateMatrix,
    zeroMatrix,
    isStringRotation,
  )
where

import Data.Char (toLower)
import Data.Foldable (foldl', toList)
import Data.Functor (($>))
import Data.List (isInfixOf)
import Data.Map.Strict (Map, insertWith)
import qualified Data.Map.Strict as M
import Data.Sequence (Seq, mapWithIndex)
import qualified Data.Sequence as Seq
import Data.Set (insert, member)

allUniques :: String -> Bool
allUniques str =
  let allUniques' knownChars i
        | i >= length str = True
        | member (str !! i) knownChars = False
        | otherwise = allUniques' (insert (str !! i) knownChars) (i + 1)
   in allUniques' mempty 0

allUniquesNoSet :: String -> Bool
allUniquesNoSet str =
  let exists char i
        | i >= length str = False
        | str !! i == char = True
        | otherwise = exists char (i + 1)
      allUniques' i
        | i >= length str = True
        | exists (str !! i) (i + 1) = False
        | otherwise = allUniques' (i + 1)
   in allUniques' 0

checkPermutations :: String -> String -> Bool
checkPermutations str1 str2
  | length str1 /= length str2 = False
  | otherwise = buildLetters str1 == buildLetters str2

buildLetters :: String -> Map Char Int
buildLetters str =
  let folder memo char = insertWith (+) char 1 memo
   in foldl' folder mempty str

urlify :: String -> Int -> String
urlify str l =
  let nbSpaces = countSpaces 0 0
      countSpaces c i
        | i >= l = c
        | str !! i == ' ' = countSpaces (c + 1) (i + 1)
        | otherwise = countSpaces c (i + 1)
      replaceChar s i char =
        let (before, after) = splitAt i s
         in before <> [char] <> drop 1 after
      urlify' :: String -> Int -> Int -> String
      urlify' s offset i
        | i < 0 = s
        | s !! i == ' ' =
          let newStr =
                replaceChar
                  ( replaceChar
                      (replaceChar s (i + offset - 2) '%')
                      (i + offset - 1)
                      '2'
                  )
                  (i + offset)
                  '0'
           in urlify' newStr (offset - 2) (i - 1)
        | otherwise =
          let newStr = replaceChar s (i + offset) (s !! i)
           in urlify' newStr offset (i - 1)
   in urlify' str (nbSpaces * 2) (l - 1)

isPalindromePermutation :: String -> Bool
isPalindromePermutation str =
  let lettersCount = buildLetters $ toLower <$> filter (/= ' ') str
      oddLettersCount = length $ M.filter odd lettersCount
   in oddLettersCount == 0 || oddLettersCount == 1

areOneEditAway :: String -> String -> Bool
areOneEditAway str1 str2
  | length str1 == length str2 = areOneReplaceAway str1 str2 False
  | length str1 == length str2 + 1 = areOneRemovalAway str2 str1 False
  | length str1 + 1 == length str2 = areOneRemovalAway str1 str2 False
  | otherwise = False
  where
    areOneReplaceAway s1 s2 alreadyReplaced = case (s1, s2) of
      ([], []) -> True
      (a : ss1, b : ss2) | a == b -> areOneReplaceAway ss1 ss2 alreadyReplaced
      (_ : ss1, _ : ss2) -> not alreadyReplaced && areOneReplaceAway ss1 ss2 True
      ([], _) -> False -- Should never happen, both strings have same length
      (_, []) -> False -- Should never happen, both strings have same length
    areOneRemovalAway s1 s2 alreadyRemoved = case (s1, s2) of
      ([], []) -> True
      ([], [_]) -> not alreadyRemoved
      (a : ss1, b : ss2) | a == b -> areOneRemovalAway ss1 ss2 alreadyRemoved
      (ss1, _ : ss2) -> not alreadyRemoved && areOneRemovalAway ss1 ss2 False
      (_, []) -> False -- Should never happen because s2 is longer than s1

compress :: String -> String
compress str = compress' "" 0
  where
    compress' cStr i
      | i >= length str = if length cStr < length str then cStr else str
      | otherwise =
        let count = length $ takeWhile (== (str !! i)) $ drop i str
         in compress' (cStr <> [str !! i] <> show count) (i + count)

useSeqMatrix :: (Seq (Seq a) -> Seq (Seq a)) -> [[a]] -> [[a]]
useSeqMatrix f = fmap toList . toList . f . fmap Seq.fromList . Seq.fromList

rotateMatrix :: [[a]] -> [[a]]
rotateMatrix = useSeqMatrix rotateMatrix'

rotateMatrix' :: Seq (Seq a) -> Seq (Seq a)
rotateMatrix' matrix =
  let size = length matrix
      colMapper x = mapWithIndex (\y _ -> rowMapper x y)
      rowMapper x y = matrix `Seq.index` (size - y - 1) `Seq.index` x
   in mapWithIndex colMapper matrix

zeroMatrix :: [[Int]] -> [[Int]]
zeroMatrix = useSeqMatrix zeroMatrix'

zeroMatrix' :: Seq (Seq Int) -> Seq (Seq Int)
zeroMatrix' matrix =
  let width = length matrix
      height = length $ matrix `Seq.index` 0
      zeroMatrix'' x y result
        | y == height = result
        | x == width = zeroMatrix'' 0 (y + 1) result
        | matrix `Seq.index` x `Seq.index` y == 0 = zeroMatrix'' (x + 1) y $ nullifyRowCol x y result
        | otherwise = zeroMatrix'' (x + 1) y result
   in zeroMatrix'' 0 0 matrix

nullifyRowCol :: Int -> Int -> Seq (Seq Int) -> Seq (Seq Int)
nullifyRowCol x y matrix =
  let colMapper :: Int -> Seq Int -> Seq Int
      colMapper x' col =
        if x' == x
          then col $> 0
          else mapWithIndex rowMapper col
      rowMapper :: Int -> Int -> Int
      rowMapper y' v =
        if y' == y
          then 0
          else v
   in mapWithIndex colMapper matrix

isStringRotation :: String -> String -> Bool
isStringRotation str1 str2
  | length str1 /= length str2 = False
  | otherwise = isInfixOf str2 $ str1 <> str1
