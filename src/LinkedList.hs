{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE TypeFamilies #-}

module LinkedList
  ( Node (..),
    reverseList,
    removeDup,
    removeDupNoBuff,
    kToLast,
    kToLastRunner,
    partition,
    sumLists,
    sumListsInOrder,
    isPalindrome,
    isPalindromeFast,
  )
where

import Control.Monad (join)
import Data.Maybe (fromMaybe)
import qualified Data.Set as Set
import GHC.Exts (IsList (..))

data Node a = Node
  { value :: a,
    next :: Maybe (Node a)
  }
  deriving (Show, Eq)

instance IsList (Node a) where
  type Item (Node a) = a

  fromList :: [Item (Node a)] -> Node a
  fromList [a] = Node {value = a, next = Nothing}
  fromList (a : as) = Node {value = a, next = Just $ fromList as}
  fromList [] = errorWithoutStackTrace "Node.fromList: empty list"

  toList :: Node a -> [Item (Node a)]
  toList Node {value, next} = maybe [value] (\n -> value : toList n) next

reverseList :: Node a -> Node a
reverseList node@Node {next} =
  let head_ = node {next = Nothing}
   in maybe node (reverseList' head_) next

reverseList' :: Node a -> Node a -> Node a
reverseList' head_ current@Node {next} =
  let newHead = current {next = Just head_}
   in maybe newHead (reverseList' newHead) next

removeDup :: Ord a => Node a -> Node a
removeDup head_ =
  let removeDup' presents = \case
        Node {value, next} | Set.member value presents -> removeDup' presents =<< next
        n@Node {value, next} -> Just n {next = removeDup' (Set.insert value presents) =<< next}
   in fromMaybe head_ $ removeDup' mempty head_

removeDupNoBuff :: Ord a => Node a -> Node a
removeDupNoBuff head_ =
  let removeDup' = \case
        Node {value, next = Just n} | hasValue value n -> removeDup' n
        n@Node {next} -> Just n {next = removeDup' =<< next}
      hasValue val Node {value, next} = val == value || maybe False (hasValue val) next
   in fromMaybe head_ $ removeDup' head_

kToLast :: Int -> Node a -> Maybe (Node a)
kToLast k head_ =
  let getLength i Node {next} = maybe (i + 1) (getLength (i + 1)) next
      length_ = getLength 0 head_
      getElem i n@Node {next} = if i == 0 then Just n else getElem (i - 1) =<< next
   in getElem (length_ - k - 1) head_

kToLastRunner :: Int -> Node a -> Maybe (Node a)
kToLastRunner k head_ =
  let getElem Node {next = runnerNext} current@Node {next} =
        maybe (Just current) (\runner -> getElem runner =<< next) runnerNext
      getRunnerStart i n@Node {next} = if i == k then Just n else getRunnerStart (i + 1) =<< next
   in join $ getElem <$> getRunnerStart 0 head_ <*> Just head_

partition :: Ord a => a -> Node a -> Node a
partition a head_ =
  let partition' res node@Node {value, next} =
        if value < a
          then Just $ node {next = maybe res (partition' res) next}
          else maybe res (partition' (Just $ node {next = res})) next
   in fromMaybe head_ $ partition' Nothing head_

sumLists :: Node Int -> Node Int -> Node Int
sumLists lst1 lst2 =
  let sumLists' Nothing Nothing 0 = Nothing
      sumLists' Nothing Nothing carry = Just $ Node {value = carry, next = Nothing}
      sumLists' Nothing node carry = sumLists' node Nothing carry
      sumLists' (Just Node {value, next}) Nothing carry =
        let (newValue, newCarry) = if value + carry > 9 then (value + carry - 10, 1) else (value + carry, 0)
         in Just $ Node {value = newValue, next = sumLists' next Nothing newCarry}
      sumLists' (Just Node {value = v1, next = n1}) (Just Node {value = v2, next = n2}) carry =
        let (newValue, newCarry) = if v1 + v2 + carry > 9 then (v1 + v2 + carry - 10, 1) else (v1 + v2 + carry, 0)
         in Just $ Node {value = newValue, next = sumLists' n1 n2 newCarry}
   in fromMaybe lst1 $ sumLists' (Just lst1) (Just lst2) 0

sumListsInOrder :: Node Int -> Node Int -> Node Int
sumListsInOrder lst1 lst2 =
  let getLength Node {next} = maybe 1 (\n -> 1 + getLength n) next
      length1 = getLength lst1 :: Int
      length2 = getLength lst2
      padLeft l count =
        if count == 0
          then l
          else Node {value = 0, next = Just $ padLeft l (count - 1)}
      padded1 = if length1 < length2 then padLeft lst1 (length2 - length1) else lst1
      padded2 = if length2 < length1 then padLeft lst2 (length1 - length2) else lst2
      sumLists' :: Node Int -> Node Int -> (Maybe (Node Int), Int)
      sumLists' Node {value = v1, next = n1} Node {value = v2, next = n2} =
        let (next, carry) = fromMaybe (Nothing, 0) $ sumLists' <$> n1 <*> n2
            sum_ = v1 + v2 + carry
            newValue = if sum_ > 9 then sum_ - 10 else sum_
            newCarry = if sum_ > 9 then 1 else 0
         in (Just Node {value = newValue, next}, newCarry)
      (mSummedLists, finalCarry) = sumLists' padded1 padded2
      addCarry sl = if finalCarry /= 0 then Node {value = 1, next = Just sl} else sl
   in maybe lst1 addCarry mSummedLists

isPalindrome :: Eq a => Node a -> Bool
isPalindrome lst = lst == reverseList lst

isPalindromeFast :: Eq a => Node a -> Bool
isPalindromeFast lst =
  let goToMiddle :: Maybe (Node a) -> Maybe (Node a) -> [a] -> (Maybe (Node a), [a])
      goToMiddle slow fast rev = case (slow, fast) of
        (_, Just (Node {next = Nothing})) -> (next =<< slow, rev)
        (Just s, Just f) -> goToMiddle (next s) (next =<< next f) (value s : rev)
        _ -> (slow, rev)
      areEq rev cur = case (rev, cur) of
        ([], Nothing) -> True
        (v : rs, Just (Node {value, next})) | value == v -> areEq rs next
        _ -> False
      (mMiddle, reverse_) = goToMiddle (Just lst) (Just lst) []
   in areEq reverse_ mMiddle
